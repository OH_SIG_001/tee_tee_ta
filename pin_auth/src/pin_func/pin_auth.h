#include <tee_core_api.h>

#define PIN_AUTH_IMPL(_func)                                                   \
  TEE_Result _func(uint32_t param_type, TEE_Param params[4])

PIN_AUTH_IMPL(EnrollPin);
PIN_AUTH_IMPL(GetExecutorInfo);
PIN_AUTH_IMPL(VerifyTemplateData);
PIN_AUTH_IMPL(QueryPinInfo);
PIN_AUTH_IMPL(AuthPin);
PIN_AUTH_IMPL(WriteAntiBrute);
PIN_AUTH_IMPL(DeleteTemplate);
PIN_AUTH_IMPL(GenerateAlgoParameter);
PIN_AUTH_IMPL(GetAlgoParameter);